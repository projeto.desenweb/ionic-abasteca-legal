import { Posto } from './posto';

export const POSTOS: Posto[] = [
    {
        CNPJ: '05.138.015/0001-34', 
        nome: 'Petrobras - Eixo W 405 sul',
        bandeira: 'Petrobras',
        endereco: 'SQS, Bloco A,PLL, 405 - Asa Sul - Brasília, DF, 70239-010',
        preco_gasolina: 4.59,
        preco_etanol: 3.11,
        preco_diesel: 3.09,
        responsavel: 'Felipe Smith',
        tel_01: '61 985621765',
        tel_02: '61 34293569',
        email: 'petrobras405sul@gmail.com'
    },
    {
        CNPJ: '17.298.092/0001-30', 
        nome: 'Posto de Combustível Shell',
        bandeira: 'Shell',
        endereco: 'Asa Norte SQN 306 - Asa Norte, Brasília - DF, 70297-400',
        preco_gasolina: 4.55,
        preco_etanol: 3.19,
        preco_diesel: 3.09,
        responsavel: 'Ronaldo Azevedo',
        tel_01: '61 985621765',
        tel_02: '61 34293569',
        email: 'petrobras405sul@gmail.com'
    },
    {
        CNPJ: '61.190.658/0001-06', 
        nome: 'Disbrave Combustíveis',
        bandeira: 'Petrobras',
        endereco: 'SQS, Bloco A,PLL, 405 - Asa Sul - Brasília, DF, 70239-010',
        preco_gasolina: 4.63,
        preco_etanol: 3.19,
        preco_diesel: 3.09,
        responsavel: 'Felipe Smith',
        tel_01: '61 985621765',
        tel_02: '61 34293569',
        email: 'petrobras405sul@gmail.com'
    },
    {
        CNPJ: '05.138.015/0001-34', 
        nome: 'Fratelli Posto de Combustíveis',
        bandeira: 'Esso',
        endereco: 'SQS, Bloco A,PLL, 405 - Asa Sul - Brasília, DF, 70239-010',
        preco_gasolina: 4.61,
        preco_etanol: 3.19,
        preco_diesel: 3.09,
        responsavel: 'Felipe Smith',
        tel_01: '61 985621765',
        tel_02: '61 34293569',
        email: 'petrobras405sul@gmail.com'
    },
    {
        CNPJ: '05.138.015/0001-34', 
        nome: 'V-TEX',
        bandeira: 'Shell',
        endereco: 'SQS, Bloco A,PLL, 405 - Asa Sul - Brasília, DF, 70239-010',
        preco_gasolina: 4.53,
        preco_etanol: 3.19,
        preco_diesel: 3.09,
        responsavel: 'Felipe Smith',
        tel_01: '61 985621765',
        tel_02: '61 34293569',
        email: 'petrobras405sul@gmail.com'
    }
];