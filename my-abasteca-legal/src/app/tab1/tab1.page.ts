import { Component } from '@angular/core';
import { POSTOS } from '../listaPostos';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  postos = POSTOS;
}
