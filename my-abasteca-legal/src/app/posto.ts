export class Posto{
    CNPJ: string;
    nome: string;
    bandeira: string;
    endereco: string;
    preco_gasolina: number;
    preco_etanol: number;
    preco_diesel: number;
    responsavel: string;
    tel_01: string;
    tel_02: string;
    email: string;
}