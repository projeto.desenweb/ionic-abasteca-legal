import { Component } from '@angular/core';
import { POSTOS } from '../listaPostos';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  postos = POSTOS;
  
}
